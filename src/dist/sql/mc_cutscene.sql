CREATE TABLE "Cutscene" (
	"ID"		INTEGER,
	"Name"		TEXT,
	"Tag"		TEXT,
	"Duration"	INTEGER,
	PRIMARY KEY("ID" AUTOINCREMENT)
);
