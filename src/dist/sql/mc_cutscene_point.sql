CREATE TABLE "CutscenePoint" (
	"CutsceneID"	INTEGER,
	"Point"			INTEGER,
	"X"				INTEGER,
	"Y"				INTEGER,
	"Z"				INTEGER,
	"Rx"			INTEGER,
	"Ry"			INTEGER,
	"Duration"		INTEGER
);
